console.log("Hello World!");


	let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);
		
	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;


	console.log("There are " + minutesHour * hoursDay * daysYear + " minutes in a a year.")

	let tempCelsius = 132;

	console.log("132 degrees Celcius when converted to Farenheit is " + ((tempCelsius * 1.8) + 32));

	let num7 = 165;
	console.log("The remainder of 165 divided by 8 is: " + (num7 % 8))
	console.log("Is num7 divisible by 8?");
	let isDivisibleBy8 = false;
	console.log(isDivisibleBy8);

	let num8 = 348;
	console.log("The remainder of 348 divided by 4 is: " +  (num8%4))
	console.log("Is num8 divisible by 4?");
	let isDivisibleBy4 = true;
	console.log(isDivisibleBy4);

	// end
